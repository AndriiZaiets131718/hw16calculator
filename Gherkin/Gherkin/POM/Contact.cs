﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gherkin.POM
{
    public class Contact
    {

        private IWebDriver _driver;

        public By informationAboutContact = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[1]/h3");

        public Contact(IWebDriver driver)
        {
            this._driver = driver;
        }
        //текст на страничке контактов
        public IWebElement FindInformationAboutContact()
        {
            return _driver.FindElement(informationAboutContact);
        }
        public string GetTextFromInformationAboutContact()
        {
            return FindInformationAboutContact().Text;
        }
    }
}
