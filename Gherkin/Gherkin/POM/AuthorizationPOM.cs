﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace Gherkin.POM
{
    public class AuthorizationPOM
    {
        private IWebDriver _driver;

        public By emailFieldForAuthorization = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[1]/div[1]/input");
        public By passwordFieldForAuthorization = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[2]/div[1]/input");
        public By enterButtonForAuthorization = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/button");
        //сообщение об неуспешной авторизации
        public By authenticationMessage = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[1]/div[2]/div/span");

        public AuthorizationPOM(IWebDriver driver)
        {
            this._driver = driver;
        }
        //заполняем поле email
        public AuthorizationPOM EnterTextToEmailField(string email)
        {
            _driver.FindElement(emailFieldForAuthorization).SendKeys(email);
            return this;
        }
        //заполняем поле password
        public AuthorizationPOM EnterTextToPasswordField(string password)
        {
            _driver.FindElement(passwordFieldForAuthorization).SendKeys(password);
            return this;
        }
        //нажимаем на кнопку авторизации
        public HeaderOfSitePOM EnterButtonForAuthorization()
        {
            _driver.FindElement(enterButtonForAuthorization).Click();
            return new HeaderOfSitePOM(_driver);
        }
        //сообщение авторизации
        public IWebElement FindAuthenticationMessage()
        {
            return _driver.FindElement(authenticationMessage);
        }
        public string GetTextFromAuthenticationMessage()
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.TextToBePresentInElementLocated(authenticationMessage, "Користувач з email 18@n.com не зареєстрований. Будь ласка, зареєструйтесь чи авторизуйтесь за допомогою номеру телефона."));
            _driver.FindElement(authenticationMessage).Click();
            return FindAuthenticationMessage().Text;
        }


    }
}
