﻿using Gherkin.Steps;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gherkin.POM
{
    public class Delivery
    {
        private IWebDriver _driver;
        public HeaderOfSitePOM headerOfSitePOM = new HeaderOfSitePOM(CartSteps.driver);

        public By infoDelivery = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div[2]/div[3]/div/h3");
        
        public Delivery(IWebDriver driver)
        {
            this._driver = driver;
        }

        //текст на страничке доставки
        public IWebElement FindInformationAboutDelivery()
        {
            return _driver.FindElement(infoDelivery);
        }
        public string GetTextFromInformationAboutDelivery()
        {
            return FindInformationAboutDelivery().Text;
        }
    }
}
