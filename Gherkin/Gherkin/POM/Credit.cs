﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gherkin.POM
{
    public class Credit
    {
        private IWebDriver _driver;

        public By informationAboutCredit = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/h4");

        public Credit(IWebDriver driver)
        {
            this._driver = driver;
        }
        //текст на страничке кредита
        public IWebElement FindInformationAboutCredit()
        {
            return _driver.FindElement(informationAboutCredit);
        }
        public string GetTextFromInformationAboutCredit()
        {
            return FindInformationAboutCredit().Text;
        }
    }
}
