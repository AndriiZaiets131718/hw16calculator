﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;

namespace Gherkin.POM
    
{
    public class HeaderOfSitePOM
    {
        public IWebDriver _driver;
        Actions actions;

        //кнопка входа в аккаунт
        public By enterButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div/button[1]");
        public By CartButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[6]/div/div[1]");
        //кнопка корзины
        public By CartFullButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[6]/div/div[1]");
        public By CartFullButtonText = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[6]/div/div[2]/p");
        //кнопка добавления товара в корзину
        public By addProductToCart = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[3]/div[2]/div[1]/div[2]/div[2]/button");
        //кнопка списка магазинов
        public By storeButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[4]/a");
        //поле ввода email для подписки
        public By emailFieldForSubscription = By.Id("catalog-social-block-email");
        //кнопка подписки
        public By subscriptionButton = By.XPath("/html/body/div[1]/div/div/div[3]/div/div[1]/div/div/div/button");
        //сообщение об успешной подписке
        public By successfulSubscription  = By.XPath("/html/body/div[1]/div/div/div[3]/div/div[1]/div/div/p");
        //сообщение об неуспешной подписке
        public By notSuccessfulSubscription = By.XPath("/html/body/div[1]/div/div/div[3]/div/div[1]/div/div/div/div/div[2]/span/span/span");
        //body
        public By body = By.XPath("/html/body");
        //кнопка доставки
        public By deliveryButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[5]/a");
        //кнопка кредита
        public By creditButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[6]/a");
        //кнопка контактов
        public By contactButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[8]/a");

        public HeaderOfSitePOM(IWebDriver driver)
        {
            this._driver = driver;
            actions = new Actions(driver);
        }
        public IWebElement FindEnterButton()
        {
            return _driver.FindElement(enterButton);
        }
        public string GetTextFromFindEnterButton()
        {
            return FindEnterButton().Text;
        }
       
        //переход на страничку корзины
        public CartPOM GetTextFromEmptyCartButton()
        {
            _driver.FindElement(CartButton).Click();
            return new CartPOM(_driver);
        }
        public CartPOM ClickOnFullCart()
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.TextToBePresentInElementLocated(CartFullButtonText, "Ваша корзина"));
            _driver.FindElement(CartFullButton).Click();
            return new CartPOM(_driver);
        }
        //нажимаем на кнопку аторизации в окне авторизации 
        public AuthorizationPOM ClickEnterButton()
        {
            _driver.FindElement(enterButton).Click();
            return new AuthorizationPOM(_driver);
        }
        //список магазинов
        public Stores ClickOnStoreButton()
        {
            
            _driver.FindElement(storeButton).Click();
            return new Stores(_driver);
        }
        //оформляем подписку на новости
        public HeaderOfSitePOM ScrollToField()
        {
            _driver.FindElement(body).SendKeys(Keys.Space);
            return this;
        }
        public HeaderOfSitePOM EnterTextToEmailSubscribeField(string email)
        {
            actions.MoveToElement(_driver.FindElement(emailFieldForSubscription)).Click().Build().Perform();
            _driver.FindElement(emailFieldForSubscription).SendKeys(email);
            return this;
        }
        public HeaderOfSitePOM EnterClickToEmailSubscribe()
        {
            actions.MoveToElement(_driver.FindElement(subscriptionButton), 60, 20).Click().Build().Perform();
            return this;
        }
        //проверка результата успешной подписки 

        public IWebElement FindSuccessfulSubscription()
        {
            return _driver.FindElement(successfulSubscription);
        }
        public string GetSuccessfulSubscription()
        {
            return FindSuccessfulSubscription().Text;
        }
        //проверка результата неуспешной подписки
        public IWebElement FindNotSuccessfulSubscription()
        {
            return _driver.FindElement(notSuccessfulSubscription);
        }
        public string GetNotSuccessfulSubscription()
        {
            return FindNotSuccessfulSubscription().Text;
        }
        //кнопка доставки
        public Delivery ClickOnDeliveryButton()
        {
            _driver.FindElement(deliveryButton).Click();
            return new Delivery(_driver);
        }
        //кнопка кредита
        public Delivery ClickOnCreditButton()
        {
            _driver.FindElement(deliveryButton).Click();
            return new Delivery(_driver);
        }
        //кнопка поддержки
        public Contact ClickOnCreditContact()
        {
            _driver.FindElement(contactButton).Click();
            return new Contact(_driver);
        }


    }
}