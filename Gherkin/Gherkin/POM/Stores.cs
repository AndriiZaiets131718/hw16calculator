﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gherkin.POM
{
    public class Stores
    {
        private IWebDriver _driver;

        public By informationAboutStore = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[3]/div/div/table/tbody/tr[2]/td[2]");
        
        public Stores(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindInformationAboutStore()
        {
            return _driver.FindElement(informationAboutStore);
        }
        public string GetTextFromInformationAboutStore()
        {
            return FindInformationAboutStore().Text;
        }




    }

}
