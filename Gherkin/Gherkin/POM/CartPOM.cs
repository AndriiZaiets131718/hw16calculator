﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace Gherkin.POM
{
    public class CartPOM
    {
        private IWebDriver _driver;

        public By messagesCartIsEmpty = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[1]");
        public By nameOfProductInCart = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[1]/div[2]/div[1]/a/p/span");

        public CartPOM(IWebDriver driver)
        {
            this._driver = driver;
        }

        //получаем текст что корзина пустая
        public IWebElement FindTextCartIsEmpty()
        {
            return _driver.FindElement(messagesCartIsEmpty);
        }
        public string GetTextFromTextCartIsEmpty()
        {
            return FindTextCartIsEmpty().Text;
        }
        //название товара в корзине
        public IWebElement FindProductInCart()
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.TextToBePresentInElementLocated(nameOfProductInCart, "Xiaomi Redmi Note 10 Pro 6/128 Onyx Gray"));
            return _driver.FindElement(nameOfProductInCart);
        }
        public string GetProductInCart()
        {
            return FindProductInCart().Text;
        }


    }
}
