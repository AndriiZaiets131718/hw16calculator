﻿using Gherkin.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

namespace Gherkin.Steps
{
    [Binding]
    public class MailSubscriptionSteps
    {
        public IWebDriver driver;
        public HeaderOfSitePOM headerOfSitePOM = new HeaderOfSitePOM(CartSteps.driver);

        [When(@"User enter valid email and press subscribe")]
        public void WhenUserEnterValidEmailAndPressSubscribe()
        {
            headerOfSitePOM.ScrollToField()
                           .EnterTextToEmailSubscribeField("test@gmail.com")
                           .EnterClickToEmailSubscribe();
        }
        [Then(@"There are messages that user signed successfully")]
        public void ThenThereAreMessagesThatUserSignedSuccessfully()
        {
            string successfulSubscriptionMessage = headerOfSitePOM.GetSuccessfulSubscription();
            Assert.AreEqual(successfulSubscriptionMessage, "Лист з посиланням для підтвердження підписки відіслано на вашу адресу.");
        }

        [When(@"User enter not valid email and press subscribe")]
        public void WhenUserEnterNotValidEmailAndPressSubscribe()
        {
           headerOfSitePOM.ScrollToField()
                           .EnterTextToEmailSubscribeField("test")
                           .EnterClickToEmailSubscribe();
        }
        
        [Then(@"There are messages that user doesn't signed successfully")]
        public void ThenThereAreMessagesThatUserDoesnTSignedSuccessfully()
        {
            string notSuccessfulSubscriptionMessage = headerOfSitePOM.GetNotSuccessfulSubscription();
            Assert.AreEqual(notSuccessfulSubscriptionMessage, "Будь ласка, вкажіть коректну адресу електронної пошти");

        }
    }
}
