﻿using Gherkin.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace Gherkin.Steps
{
    [Binding]
    public class InformationSteps
    {
        public IWebDriver driver;
        public HeaderOfSitePOM headerOfSitePOM;
        public Stores stores;
        public Delivery delivery;
        public Credit credit;
        public Contact contact;

        //check information about address
        [When(@"User clicks on store button")]
        public void WhenUserClicksOnStoreButton()
        {
            headerOfSitePOM = new HeaderOfSitePOM(CartSteps.driver);
            headerOfSitePOM.ClickOnStoreButton();
        }
        [Then(@"There are list of stores with address")]
        public void ThenThereAreListOfStoresWithAddress()
        {
            stores = new Stores(driver);
            string informationAboutStore = stores.GetTextFromInformationAboutStore();
            Assert.AreEqual(informationAboutStore, "просп. Правды, 47, ТРЦ «Retroville», просп. Правды, 47, маг. Алло MAX");
        }
        //check information about delivery
        [When(@"User go to page with information about delivery")]
        public void WhenUserGoToPageWithInformationAboutDelivery()
        {
            headerOfSitePOM = new HeaderOfSitePOM(CartSteps.driver);
            headerOfSitePOM.ClickOnDeliveryButton();
        }
        
        [Then(@"User can check that information")]
        public void ThenUserCanCheckThatInformation()
        {
            delivery = new Delivery(driver);
            string infodelivery = delivery.GetTextFromInformationAboutDelivery();
            Assert.AreEqual(infodelivery, "Як оформити замовлення?");
        }
        //check information about credit
        [When(@"User go to page with information about credit")]
        public void WhenUserGoToPageWithInformationAboutCredit()
        {
            headerOfSitePOM = new HeaderOfSitePOM(CartSteps.driver);
            headerOfSitePOM.ClickOnCreditButton();
        }

        [Then(@"User can check information about credit")]
        public void ThenUserCanCheckInformationAboutCredit()
        {
            credit = new Credit(driver);
            string infocredit = credit.GetTextFromInformationAboutCredit();
            Assert.AreEqual(infocredit, "Детальна інформація про умови кредитування:");
        }

        //check information about contacts information
        [When(@"User go to page with contacts information")]
        public void WhenUserGoToPageWithContactsInformation()
        {
            headerOfSitePOM = new HeaderOfSitePOM(CartSteps.driver);
            headerOfSitePOM.ClickOnCreditContact();


        }

        [Then(@"User can choose optimal number for call to support")]
        public void ThenUserCanChooseOptimalNumberForCallToSupport()
        {
            contact = new Contact(driver);
            string infocontact = contact.GetTextFromInformationAboutContact();
            Assert.AreEqual(infocontact, "Консультації та замовлення за телефонами");
        }

    }
}
