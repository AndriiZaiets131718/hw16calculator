﻿using Gherkin.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace Gherkin.Steps
{
    [Binding]
    public class CartSteps
    {
        public static IWebDriver driver;
        public static HeaderOfSitePOM headerOfSitePOM;
        public static CartPOM cartPOM;
        public static AuthorizationPOM authorizationPOM;

        [Before]
        public void CreateDriver()
        {
            driver = new ChromeDriver(@"D:\Selenium");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            headerOfSitePOM = new HeaderOfSitePOM(driver);
        }

        // Unauthorized user has empty cart
        [Given(@"Allo website is open")]
        public static void GivenAlloWebsiteIsOpen()
        {
            driver.Navigate().GoToUrl("https://allo.ua/");
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);
            driver.Manage().Window.Maximize();
        }
       
        [Given(@"User is not logged in")]
        public void GivenUserIsNotLoggedIn()
        {
            string enterButtonText = headerOfSitePOM.GetTextFromFindEnterButton();
            Assert.AreEqual("Вхід", enterButtonText);
        }
        [When(@"User click on cart")]
        public void WhenUserClickOnCart()
        {
            //переходим на страничку пустой корзины
            cartPOM = headerOfSitePOM.GetTextFromEmptyCartButton(); 
        }
        [Then(@"Cart is empty")]
        public void ThenCartIsEmpty()
        {
            string textEmptyCart = cartPOM.GetTextFromTextCartIsEmpty();
            Assert.AreEqual("Ваш кошик порожній.", textEmptyCart);
        }

        //Authorized user can see saved products
        [When(@"User is  logged in")]
        public void WhenUserIsLoggedIn()
        {
            authorizationPOM = headerOfSitePOM.ClickEnterButton();
            authorizationPOM.EnterTextToEmailField("baneyev818@nnacell.com").EnterTextToPasswordField("111111").
            EnterButtonForAuthorization();
        }

        [Then(@"Check saved products in cart")]
        public void ThenCheckSavedProductsInCart()
        {
            cartPOM = headerOfSitePOM.ClickOnFullCart();
            string nameProductInCart = cartPOM.GetProductInCart();
            Assert.AreEqual(nameProductInCart, "Xiaomi Redmi Note 10 Pro 6/128 Onyx Gray");
        }
        //User with not valid data can't get access to account
        [When(@"User try to log in with not valid data")]
        public void WhenUserTryToLogInWithNotValidData()
        {
            authorizationPOM = headerOfSitePOM.ClickEnterButton();
            authorizationPOM.EnterTextToEmailField("18@n.com").EnterTextToPasswordField("011111").
            EnterButtonForAuthorization();
        }

        [Then(@"Check that user with not valid data doesn't get access to account")]
        public void ThenCheckThatUserWithNotValidDataDoesnTGetAccessToAccount()
        {
            string textAuthenticationMessage = authorizationPOM.GetTextFromAuthenticationMessage();
            Assert.AreEqual(textAuthenticationMessage, "Користувач з email 18@n.com не зареєстрований. Будь ласка, зареєструйтесь чи авторизуйтесь за допомогою номеру телефона.");
        }





    }
}
