﻿Feature: Cart
 As a user
 I want to access to my cart
 In order to view a content of my order any time

 As a user 
 I want to authorized
 So that I can see products saved by me

 As a user 
 I want to be sure that I can log into my account only after successful authorization
 So that I will be sure that no one will be able to make purchases with my name


Scenario: Unauthorized user has empty cart
 Given Allo website is open 
 Given User is not logged in 
 When User click on cart 
 Then Cart is empty

 Scenario: Authorized user can see saved products
 Given Allo website is open
 When User is  logged in
 Then Check saved products in cart

 Scenario: User with not valid data can't get access to account
 Given Allo website is open
 When User try to log in with not valid data
 Then Check that user with not valid data doesn't get access to account
