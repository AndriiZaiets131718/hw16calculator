﻿Feature: mailSubscription
As I user
I want to subscribe to mailing
So that I can get news about company

As I admin
I want to have verification for email on my site
So that I can get information only about exist emails

As I user 
I want to read information about delivery
So that I can choose a convenient delivery method

As I user
I want to read information about credit
So that I can choose optimal loan option

As I user
I want to read information about Contacts
So that I can choose optimal number for call to support

As I user
I want to read information about warranty
so that I can know information about warranty

Scenario: Subscription with valid email
	Given Allo website is open
	When User enter valid email and press subscribe
	Then There are messages that user signed successfully

Scenario: Subscription with not valid email
    Given Allo website is open
    When User enter not valid email and press subscribe
    Then There are messages that user doesn't signed successfully

Scenario: User read information about delivery
	Given Allo website is open
	When User go to page with information about delivery
	Then User can check that information

Scenario: User read information about credit
	Given Allo website is open
	When User go to page with information about credit
	Then User can check information about credit

Scenario: User read information about contacts
	Given Allo website is open
	When User go to page with contacts information
	Then User can choose optimal number for call to support

Scenario: User read information about warranty
	Given Allo website is open
	When User go to page about warranty
	Then User can check information about warranty